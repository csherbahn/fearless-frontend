import React from "react";

class ConferenceForm extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      name: "",
      starts: '',
      ends: '',
      max_presentations: '',
      max_attendees: '',
      description: '',
      locations: [],
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }
  handleChange(event) {
    const value = event.target.value;
    const key = event.target.name;
    const changeDict = {}
    changeDict[key] = value
    this.setState(changeDict);
  }
  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.locations

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': "application/json",
      },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();

      const cleared = {
        name: '',
        starts: '',
        ends: '',
        max_presentations: '',
        max_attendees: '',
        description: '',
        location: '',
      };
      this.setState(cleared);
    }
  }
  async componentDidMount() {
    const url = "http://localhost:8000/api/locations/";

    const response = await fetch(url)

    if (response.ok){
      const data = await response.json()
      this.setState({locations: data.locations})
    }
  }
  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.name} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.starts} placeholder="Start Date" required type="date" id="starts" name="starts" className="form-control" />
                <label htmlFor="starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.ends} placeholder="End Date" required type="date" id="ends" name="ends" className="form-control" />
                <label htmlFor="ends">End Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.max_presentations} placeholder="Max Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.max_attendees} placeholder="Max Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={this.handleChange} value={this.state.description} id="description" name="description" rows="7" cols="50" placeholder="Write your description here..." className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChange} value={this.state.location} required id="location" name="location" className="form-select">
                  <option value="">Choose a Location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                    );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default ConferenceForm;